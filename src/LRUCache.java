import java.util.LinkedHashMap;
import java.util.Map;


public class LRUCache {
	int capacity = -1;
	LinkedHashMap<Integer, Integer> cache;
	
	public LRUCache(int capacity) {
		this.capacity = capacity;
	}
	public int get(int key) {
		if (!cache.containsKey(key)) return -1;
		return cache.get(key);
	}
	
	public void set(int key, int value) {
		cache.put(key, value);
	}
	
	public static void main(String[] args) {
		LRUCache c = new LRUCache(2);
		c.set(1, 1);
		System.out.println("set(1, 1)");
		System.out.println(c.cache);
		c.set(2, 1);
		System.out.println("set(2, 1)");
		System.out.println(c.cache);
		c.get(1);
		System.out.println("get(1)");
		System.out.println(c.cache);
		c.set(3, 1);
		System.out.println("set(3, 1)");
		System.out.println(c.cache);
	}
}
