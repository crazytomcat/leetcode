
public class ListCycle {
	public ListNode detectCycle(ListNode head) {
		if (head == null || head.next == null) return null;
        ListNode slower = head;
        ListNode faster = head;
        do {
        	slower = slower.next;
        	if (slower == null) return null;
        	if (faster.next == null) return null;
        	faster = faster.next.next;
        	if (faster == null) return null;
        } while (slower != faster);
        
        slower = head;
        while (slower != faster) {
        	slower = slower.next;
        	faster = faster.next;
        }
        return slower;
    }
	
	public boolean hasCycle(ListNode head) {
		if (head == null || head.next == null) return false;
        ListNode slower = head;
        ListNode faster = head.next;
        while (slower != null) {
        	if (slower == faster) return true;
        	slower = slower.next;
        	if (faster.next == null) return false;
        	faster = faster.next.next;
        	if (faster == null) return false;
        }
        return false;
    }
	
	public static void main(String[] args) {
		ListCycle l = new ListCycle();
		ListNode head = new ListNode(1);
		ListNode n2 = new ListNode(2);
		ListNode n3 = new ListNode(3);
		ListNode n4 = new ListNode(4);
		ListNode n5 = new ListNode(5);
		ListNode n6 = new ListNode(6);
		head.next = n2;
		n2.next = n3;
		n3.next = n4;
		n4.next = n5;
		n5.next = n6;
		n6.next = n3;
		ListNode a = l.detectCycle(head);
		if (a != null) System.out.println(a.val);
	}
}
