import java.util.LinkedList;


public class LRUCache2 {
	LinkedList<int[]> cache;
	int capacity = -1;
	public LRUCache2(int capacity) {
		cache = new LinkedList<int[]>();
		this.capacity = capacity;
    }
    
    public int get(int key) {
    	int[] data;
    	int idx = -1;
    	for (int i = 0; i < cache.size(); i++) {
    		data = cache.get(i);
    		if (data[0] == key) { 
    			idx = i;
    			break;
    		}
    	}
    	if (idx == -1) return -1;
    	data = cache.remove(idx);
    	cache.addLast(data);
    	return data[1];
    }
    
    public boolean doUpdate(int[] data) {
    	int idx = -1;
    	for (int i = 0; i < cache.size(); i++) {
    		if (cache.get(i)[0] == data[0]) {
    			idx = i;
    			break;
    		}
    	}
    	if (idx == -1) return false;
    	cache.remove(idx);
    	cache.addLast(data);
    	return true;
    }
    
    public void set(int key, int value) {
   		int[] data = {key, value};
   		if (doUpdate(data)) return;
    	if (cache.size() < capacity) {
    		cache.addLast(data);
    	} else {
    		cache.removeFirst();
    		cache.addLast(data);
    	}
    }
}
