import java.util.*;

public class WordBreak {
	
//	public ArrayList<String> wordBreak(String s, Set<String> dict, String answer) {
//		if (s.length() == 0) {
//			result.add(answer.trim());
//			return result;
//		}
//		
//		int idx;
//		for (String d: dict) {
//			idx = s.indexOf(d);
//			if (idx == 0) {
//				String str = s.substring(d.length(), s.length());
//				wordBreak(str, dict, answer + " " + d);
//			}
//		}
//		return result;
//	}
	
	public ArrayList<String> find(String s, HashMap<Integer, ArrayList<Integer>> matchChain,
			int start, String answer, ArrayList<String> result) {
		if (matchChain.get(start) == null) return result;
		String str;
		for (Integer end : matchChain.get(start)) {
			str = s.substring(start, end);
			if (end == s.length()) {
				String a = answer + " " + str;
				result.add(a.trim());
			} else find(s, matchChain, end, answer + " " + str, result);
		}
		return result;
	}
	
	public ArrayList<String> wordBreak2(String s, Set<String> dict) {
		HashMap<Integer, ArrayList<Integer>> matchChain = new HashMap<Integer, ArrayList<Integer>>();
		int length = s.length();
		String str;
		for (int end = length; end >= 0; end--) {
			if (end < length && matchChain.get(end) == null) continue;
			for (int start = end-1; start >= 0; start--) {
				str = s.substring(start, end);
				if (dict.contains(str)) {
					if (matchChain.get(start) == null) matchChain.put(start, new ArrayList<Integer>());
					matchChain.get(start).add(end);
				}
			}
		}
//		for (Map.Entry<Integer, ArrayList<Integer>> entry : matchChain.entrySet()) {
//			System.out.println("key..." + entry.getKey());
//			System.out.println("val..." + entry.getValue());
//		}
		
		ArrayList<String> result = new ArrayList<String>();
		if (matchChain.size() == 0) return result;		
		find(s, matchChain, 0, "", result);
		return result;
	}
	
	public boolean wordBreak(String s, Set<String> dict, HashMap<String, Boolean> table) {
		if (s.length() == 0) return true;
		if (table.get(s) != null) return table.get(s);
		
		int idx;
        for (String d : dict) {
        	idx = s.indexOf(d);
//        	System.out.println(d  + ", idx = " + idx);
        	if (idx == 0) {
//        		System.out.println("-- str = " + s.substring(d.length(), s.length()));
        		String str = s.substring(d.length(), s.length());
        		if (wordBreak(str, dict, table)) {
        			table.put(str, true);
        			return true;
        		} else {
        			table.put(str, false);
        		}
        	}
        }
        return false;
		
	}
	public boolean wordBreak(String s, Set<String> dict) {
		if (s.length() == 0) return false;
		return wordBreak(s, dict, new HashMap<String, Boolean>());
    }
	
	public static void main(String[] args) {
		WordBreak wb = new WordBreak();
//		String s = "aaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaab";
		Set<String> dict = new HashSet<String>();
//		dict.add("a");
//		dict.add("aa");
//		dict.add("aaa");
//		dict.add("aaaa");
//		dict.add("aaaaa");
//		dict.add("aaaaaa");
//		dict.add("aaaaaaa");
//		dict.add("aaaaaaaa");
//		dict.add("aaaaaaaaa");
//		dict.add("aaaaaaaaaa");
		String s = "aaaaaaa";
		dict.add("aaaa");
		dict.add("aa");
		dict.add("a");
//		String s = "catsanddog";
//		dict.add("cat");
//		dict.add("cats");
//		dict.add("and");
//		dict.add("sand");
//		dict.add("dog");
		System.out.println(wb.wordBreak2(s, dict));
	}
}
