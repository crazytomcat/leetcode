
public class GasStation {
	public int canCompleteCircuit(int[] gas, int[] cost) {
		int left = 0;
		int start = 0;
		int need = 0;
		for (int i = 0; i < gas.length; i++) {
			left += (gas[i] - cost[i]);
			if (left < 0) {
				need -= left;
				start = i + 1;
				left = 0;
			}
		}
		if (left >= need) return start;
		return -1;
	}
	
	public static void main(String[] args) {
		GasStation gs = new GasStation();
		int[] gas = {2};
		int[] cost = {2};
		System.out.println(gs.canCompleteCircuit(gas, cost));
	}
}
