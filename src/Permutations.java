import java.util.ArrayList;
import java.util.HashSet;


public class Permutations {
	boolean [] isUsed;
	int numLength;
	ArrayList<ArrayList<Integer>> output;
	ArrayList <Integer> one;
	HashSet<String> result;
	public void doPermute(int index, int[] num) {
//		System.out.println("index = " + index);
//		for (int i = 0; i < numLength; i++) System.out.print(isUsed[i] + " ");
//		System.out.println();
		
		if (index == numLength) {
//			System.out.println("add = " + one);
			output.add((ArrayList<Integer>)one.clone());
			return;
		}
		
		for (int i = 0; i < numLength; i++) {
			if (!isUsed[i]) {
				one.add(num[i]);
				isUsed[i] = true;
				
//				System.out.println("index = " + index + ", i = " + i + ", Before...");
//				for (int j = 0; j < numLength; j++) System.out.print(isUsed[j] + " ");
//				System.out.println();
//				
				doPermute(index+1, num);		
				isUsed[i] = false;
				
//				System.out.println("index = " + index + ", i = " + i + ", After...");
//				for (int j = 0; j < numLength; j++) System.out.print(isUsed[j] + " ");
//				System.out.println();
//				System.out.println("----------------------");
				
				one.remove(index);
			}
		}
	}
	
	public ArrayList<ArrayList<Integer>> permute(int[] num) {
		numLength = num.length;
		one = new ArrayList <Integer>();
		output = new ArrayList<ArrayList<Integer>>();
		isUsed = new boolean[num.length];
		doPermute(0, num);
        return output;
    }
	
	public void doPermuteUniqle(int index, int[] num, String per) {
//		System.out.println("index = " + index);
//		for (int i = 0; i < numLength; i++) System.out.print(isUsed[i] + " ");
//		System.out.println();
		
		if (index == numLength) {
//			System.out.println("add = " + one);
//			System.out.println("add = " + per.trim());
			per = per.trim();
			System.out.println("per = " + per);
			System.out.println("result = " + result);
			if (!result.contains(per)) {
				output.add((ArrayList<Integer>)one.clone());
				result.add(per);
			}
			return;
		}
		
		for (int i = 0; i < numLength; i++) {
			if (!isUsed[i]) {
//				per += num[i] + " ";
				one.add(num[i]);
				isUsed[i] = true;
				
//				System.out.println("index = " + index + ", i = " + i + ", Before...");
//				for (int j = 0; j < numLength; j++) System.out.print(isUsed[j] + " ");
//				System.out.println();
//				
				doPermuteUniqle(index+1, num, per + num[i] + " ");		
				isUsed[i] = false;
				
//				System.out.println("index = " + index + ", i = " + i + ", After...");
//				for (int j = 0; j < numLength; j++) System.out.print(isUsed[j] + " ");
//				System.out.println();
//				System.out.println("----------------------");
				
				one.remove(index);
			}
		}
	}
	
	public ArrayList<ArrayList<Integer>> permuteUnique(int[] num) {
		numLength = num.length;
		isUsed = new boolean[num.length];
		output = new ArrayList<ArrayList<Integer>>();
		one = new ArrayList<Integer>();
		result = new HashSet<String>();
		doPermuteUniqle(0, num, "");
//		System.out.println("result = " + result);
//		output = new ArrayList<ArrayList<Integer>>();
//		String[] tmp;
//		ArrayList<Integer> one;
//		for (String r : result) {
//			tmp = r.split(" ");
//			one = new ArrayList<Integer>();
//			for (String s : tmp) one.add(Integer.parseInt(s));
//			output.add(one);
//		}
        return output;
	}
	
	public static void main(String[] args) {
		Permutations p = new Permutations();
		int[] num = {3, 3, 0, 0, 2, 3, 2};
//		int[] num= {1, 1, 2};
		System.out.println(p.permuteUnique(num));
	}
}
