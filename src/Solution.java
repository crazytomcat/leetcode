import java.io.BufferedReader;
import java.io.FileReader;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.HashSet;
import java.util.Stack;

class Point {
	int x;
	int y;
	Point() { x = 0; y = 0; }
	Point(int a, int b) { x = a; y = b; }
}

class TreeNode {
	int val;
	TreeNode left;
	TreeNode right;
	TreeNode(int x) { val = x; }
}
class ListNode {
	int val;
	ListNode next;
	ListNode(int x) {
		val = x;
	 	next = null;
	}
}

class UndirectedGraphNode {
	int label;
	ArrayList<UndirectedGraphNode> neighbors;
	UndirectedGraphNode(int x) {
		label = x;
		neighbors = new ArrayList<UndirectedGraphNode>();
	}
};

public class Solution {
	public String reverseWords(String s) {
        s = s.trim();
        String[] tmp = s.split(" ");
        System.out.println(tmp.length);
        String r = "";
        for (int i = tmp.length-1; i >= 0; i--) {
        	if (tmp[i].isEmpty()) continue;
        	r += tmp[i] + " ";
        }
        r = r.trim();
        System.out.println(r);
        return r;
    }
	
	public int evalRPN(String[] tokens) {
        Stack<Integer> stack = new Stack<Integer>();
        int a, b;
        for (String t : tokens) {
        	if (t.equals("+")) {
        		b = stack.pop();
        		a = stack.pop();
        		stack.push(a + b);
        	} else if (t.equals("-")) {
        		b = stack.pop();
        		a = stack.pop();
        		stack.push(a - b);
        	} else if (t.equals("*")) {
        		b = stack.pop();
        		a = stack.pop();
        		stack.push(a * b);
        	} else if (t.equals("/")) {
        		b = stack.pop();
        		a = stack.pop();
        		stack.push(a / b);
        	} else {
        		a = Integer.parseInt(t);
        		stack.push(a);
        	}
        }
        return stack.pop();
    }
	
	public int gcd (int a, int b) {
		if (b == 0) return a;
		return gcd(b, a%b);
	}
	
	public int maxPoints(Point[] points) {
		int size = points.length;
		if (size <= 1) return size;
		int dx, dy;
		double m, b;
		HashSet<Point> group;
		int add, max, maxAndSame = -1;
		String id;
        for (int i = 0; i < size-1; i++) {
        	add = 0;
        	max = 0;
        	HashMap<String, HashSet<Point>> mb = new HashMap<String, HashSet<Point>>();
        	for (int j = i+1; j < size; j++) {
        		if (points[i].x == points[j].x) {
        			if (points[i].y == points[j].y) {
        				add++;
        				continue;
        			} else {
        				m = 0;
        				b = points[i].y;
        				id = "0_" + b;
        			}
        		} else {
        			m = (double) Math.abs(points[i].y - points[j].y) / Math.abs(points[i].x - points[j].x);
        			dx = points[i].x - points[j].x;
        			dy = points[i].y - points[j].y;
        			if ((dx >= 0 && dy < 0) || (dx <= 0 && dy > 0)) m = -m;
        			b = (double) points[i].y - m * points[i].x;
        			id = m + "_" + b;
        		}
        		System.out.println("id = " + id);
        		group = mb.get(id);
        		if (group == null) group = new HashSet<Point>();
        		System.out.println("group = " + group);
        		group.add(points[i]);
        		group.add(points[j]);
        		mb.put(id, group);
        		if (group.size() > max) max = group.size();
        	}
        	if (max == 0) max = add + 1;
        	else max += add;
        	
        	if (max > maxAndSame) maxAndSame = max;
        }
        return maxAndSame;
    }
	
	public int trace(TreeNode root) {
		System.out.println("root = " + root);
		if (root.left == null && root.right == null) return 1;
        int a = 0, b = 0;
        if (root.left != null) {
        	System.out.println("go trace left");
        	a = trace(root.left) + 1;
        }
        if (root.right != null) {
        	System.out.println("go trace right");
        	b = trace(root.right) + 1;
        }
        System.out.println("(a, b) = (" + a + ", " + b + ")");
        if (a >= b) return a;
        return b;
	}
	
	public int maxDepth(TreeNode root) {
		if (root == null) return 0;
		if (root.left == null && root.right == null) return 1;
        int a = 0, b = 0;
        if (root.left != null) a = trace(root.left);
        if (root.right != null) b = trace(root.right);
        System.out.println("** (a, b) = (" + a + ", " + b + ")");
        if (a >= b) return a+1;
        return b+1;
        
    }
	
//	public ListNode merge(ListNode left, ListNode right) {
//		
//	}
//	
//	public ListNode sortList(ListNode head) {
//		if (head == null || head.next == null) return head;
//		int length = 1;
//		ListNode node = head;
//		while (node.next != null) {
//			length += 1;
//			node = node.next;
//		}
//		int size = 1;
//		ListNode left = head;
//		while (size <= length) {
//			
//		}
//		
//		return head;
//    }
	
	public ListNode insertionSortList(ListNode head) {
		if (head == null || head.next == null) return head;
		ListNode current = head.next, cp = head;
		ListNode previous = head, pp = head;
		ListNode tmp;
		while (current != null) {
			previous = head;
			tmp = null;
//			System.out.println("current = " + current.val);
//			System.out.println("current.next = " + current.next.val);
//			System.out.println("cp = " + cp.val);
			while (previous != current) {
//				System.out.println("previous = " + previous.val);
				if (current.val < previous.val) {
					tmp = current.next;
					current.next = previous;
					cp.next = tmp;
					if (previous == head) head = current;
					else pp.next = current;
//					System.out.println("tmp = " + tmp.val);
//					System.out.println("head = " + head.val);
//					ListNode node = head;
//					while (node != null) {
//						System.out.print(node.val + " ");
//						node = node.next;
//					}
//					System.out.println();
					break;
				}
				pp = previous;
				previous = previous.next;
			}
			if (tmp != null) {
//				System.out.println("tmp = " + tmp.val);
				current = tmp;
			} else {
				cp = current;
				current = current.next;
			}
//			System.out.println("--------------");
		}
		return head;
    }
	
	public ListNode readSortData() {
		String[] data = null;
		try {
			BufferedReader br = new BufferedReader(new FileReader("/home/chiachun/workspace/LeetCode/data/insertion_sort.txt"));
			String row = br.readLine();
			data = row.split(",");
			br.close();
		} catch (Exception e) {
			e.printStackTrace();
		}
		ListNode head = new ListNode(Integer.parseInt(data[0]));
		ListNode node, previous = head;
		for (int i = 1; i < data.length; i++) {
			node = new ListNode(Integer.parseInt(data[i]));
			previous.next = node;
			previous = node;
		}
//		node = head;
//		while(node != null) {
//			System.out.print(node.val + " ");
//			node = node.next;
//		}
		return head;
	}
	
	public static void main(String[] args) {
		
		Solution solution = new Solution();
	}
}
