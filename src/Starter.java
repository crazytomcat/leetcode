import java.io.BufferedReader;
import java.io.FileReader;


public class Starter {
	
	public void run() throws Exception {
		BufferedReader br = new BufferedReader(new FileReader("/home/chiachun/workspace/LeetCode/data/cache_data"));
		int size = Integer.parseInt(br.readLine());
		String data = br.readLine();
		br.close();
	}
    
    public static void main(String[] args) {
    	Starter s = new Starter();
    	try {
    		s.run();
    	} catch (Exception e) {
    		e.printStackTrace();
    	}
    }
}
