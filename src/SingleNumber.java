import java.util.HashMap;
import java.util.HashSet;
import java.util.Map;


public class SingleNumber {

	public int singleNumber2(int[] A) {
        HashMap<Integer, Integer> record = new HashMap<Integer, Integer>();
        Integer j = 0;
        for (int i = 0; i < A.length; i++) {
        	j = record.get(A[i]);
        	if (j == null) record.put(A[i], 1);
        	else {
        		if (j == 2) record.remove(A[i]);
        		else record.put(A[i], j+1);
        	}
        }
        for (Map.Entry<Integer, Integer> entry : record.entrySet()) {
        	j = entry.getKey();
        }
        return j;
	}
	
	public int singleNumber(int[] A) {
		HashSet<Integer> record = new HashSet<Integer>();
		for (int i = 0; i < A.length; i++) {
			if (record.contains(A[i])) record.remove(A[i]);
			else record.add(A[i]);
		}
		int i = 0;
		for (int j : record) i = j;
		return i;
	}
	
	public static void main(String[] args) {
		SingleNumber sn = new SingleNumber();
		int[] A = {1, 1, 2, 4, 3, 2, 4, 1, 3, 2, 4};
		System.out.println(sn.singleNumber2(A));
	}
}
