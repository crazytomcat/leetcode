package leetcode;

class TreeNode {
	int val;
	TreeNode left;
	TreeNode right;
	TreeNode(int x) { val = x; }
}

public class MaximumDepthOfBinaryTree {
	public int trace(TreeNode root, int depth) {
		System.out.println(root + ", depth = " + depth);
		if (root == null) return 1;
		if (root.left != null) return (trace(root.left, depth) + depth);
		if (root.right != null) return (trace(root.right, depth) + depth);
		
		return 1;
	}
	
	public int maxDepth(TreeNode root) {
//		if (root.left == null && root.right == null) return 1;
		if (root == null) return 0;
        int a = trace(root.left, 0);
        int b = trace(root.right, 0);
        System.out.println("a = " + a + ", b = " + b);
        if (a >= b) return a;
        return b;
    }
	
	public static void main(String[] args) {
		MaximumDepthOfBinaryTree m = new MaximumDepthOfBinaryTree();
		TreeNode root = new TreeNode(0);
		root.left = new TreeNode(1);
		root.left.left = new TreeNode(2);
		root.left.right = new TreeNode(2);
		
		root.right = new TreeNode(1);
		root.right.right = new TreeNode(2);
		root.right.right.right = new TreeNode(3);
		System.out.println(m.maxDepth(root));
	}
}
