
public class Candy {
	public int candy2(int[] ratings) {
		int length = ratings.length;
		if (length <= 1) return length;
		
		int[] amount = new int[length];
		// first element
		amount[0] = 1;
		for (int i = 1; i < length; i++) {
			if (ratings[i] > ratings[i-1]) amount[i] = amount[i-1]+1;
			else if (ratings[i] == ratings[i-1]) amount[i] = amount[i-1];
			else {
				if (amount[i-1] != 1) amount[i] = amount[i-1]-1;
				else {
					amount[i] = 1;
					for (int j = i-1; j >= 0; j--) {
						if (ratings[j] > ratings[j+1]) amount[j] = amount[j+1]+1;
						else break;
					}
				}
			}
		}
		int sum = 0;
		for (int k = 0; k < length; k++) sum += amount[k];
		return sum;
	}
	
	public int candy(int[] ratings) {
		int length = ratings.length;
		int[] candies = new int[length];
		for (int i = 0; i < length; i++) candies[i] = 1;
		
		for (int i = 1; i < length; i++) {
			if (ratings[i] > ratings[i-1]) candies[i] = candies[i-1]+1;
		}
//		for (int i = 0; i < length; i++) System.out.print(candies[i] + " ");
//		System.out.println();
		int sum = candies[length-1];
		for (int i = length-2; i >= 0; i--) {
			if (ratings[i] > ratings[i+1]) {
				if (candies[i+1]+1 > candies[i]) candies[i] = candies[i+1]+1;
			}
			sum += candies[i];
		}
//		for (int i = 0; i < length; i++) System.out.print(candies[i] + " ");
//		System.out.println();
//		int sum = 0;
//		for (int i = 0; i < length; i++) sum += candies[i];
		return sum;
	}
	
	public static void main(String[] args) {
		Candy c = new Candy();
		int[] ratings = {1,3,4,3,2,1};
		System.out.println(c.candy(ratings));
	}
}
