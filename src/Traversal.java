import java.util.*;

public class Traversal {
	public ArrayList<Integer> trace(TreeNode root, ArrayList<Integer> result) {
		result.add(root.val);
		if (root.left != null) trace(root.left, result);
		if (root.right != null) trace(root.right, result);
		return result;
	}
	
	public ArrayList<Integer> postorderTraversal(TreeNode root) {
		if (root == null) return new ArrayList<Integer>();
		return trace(root, new ArrayList<Integer>());
    }
	
	public static void main(String[] args) {
		Traversal traversal = new Traversal();
		TreeNode root = new TreeNode(0);
		root.left = new TreeNode(1);
		root.left.left = new TreeNode(3);
		root.left.right = new TreeNode(4);
		root.left.left.left = new TreeNode(6);
		root.left.left.right = new TreeNode(7);
		
		root.right = new TreeNode(2);
		root.right.right = new TreeNode(5);
		root.right.right.left = new TreeNode(8);
		root.right.right.right = new TreeNode(9);
		System.out.println(traversal.postorderTraversal(root));
	}
}
